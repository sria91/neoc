# neoc — An alternative implementation of the 'C Standard Library'

The implementation status of features and functionalities is available
[here](src/README.md).

This implementation is based on the final draft of C11 standard accessible
[here](http://www.open-std.org/jtc1/sc22/WG14/www/docs/n1570.pdf).

This project is really in a very early stage. Please feel free to
[contribute](CONTRIBUTING.md) to the project following the
[code style guide](CODESTYLE.md)
